<?php

/**
 * @file
 * Includes events to be triggered during the moderation work-flow.
 *
 * When the rules module is enabled, the events included during the
 * moderation process appears to the site-builder. It gives further
 * more customization with the working logic and behavior of the
 * image approval module.
 */

/**
 * Implements hook_rules_event_info().
 */
function image_approval_rules_event_info() {
  return array(
    'image_sent_for_moderation' => array(
      'group' => t('Image Approval'),
      'label' => t('Image is sent for moderation'),
      'variables' => array(
        'current_user' => array(
          'type' => 'user',
          'label' => t('Submitting user'),
        ),
      ),
    ),
    'image_when_approved' => array(
      'group' => t('Image Approval'),
      'label' => t('Image sent is approved'),
      'variables' => array(
        'picture' => array(
          'type' => 'file',
          'label' => t('Uploaded picture'),
        ),
        'user' => array(
          'type' => 'user',
          'label' => t('Submitting user'),
        ),
        'moderator' => array(
          'type' => 'user',
          'label' => t('Moderator'),
        ),
      ),
    ),
    'image_when_disapproved' => array(
      'group' => t('Image Approval'),
      'label' => t('Image sent is disapproved'),
      'variables' => array(
        'picture' => array(
          'type' => 'file',
          'label' => t('Uploaded picture'),
        ),
        'user' => array(
          'type' => 'user',
          'label' => t('Submitting user'),
        ),
        'moderator' => array(
          'type' => 'user',
          'label' => t('Moderator'),
        ),
      ),
    ),
  );
}
