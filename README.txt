INTRODUCTION
------------
The Image Approval module allows the moderator to view the recently uploaded
images by the users for their account, they can either approve or disapprove 
the uploaded image. The approved image will be published in the user's account 
across the site.If the image is disapproved, the user will be intimated to 
upload a new image.The module also has support for rules integration.

* For a full description of the module, visit the project page:
   https://drupal.org/project/image_approval

REQUIREMENTS
------------

None.

INSTALLATION
------------
* Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
* Configure user permissions in Administration » People » Permissions:

   - Moderate user's uploaded image

     Users in roles with the "Moderate user's uploaded image" permission 
		 will see the user's uploaded image, and they have the privilege to
		 either approve or disapprove the image based on the policy across 
		 the site.  

 * Moderate user's images in 
   Administration » Configuration » People » Image Approval.
