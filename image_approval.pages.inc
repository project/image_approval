<?php

/**
 * @file
 * Page callbacks for Image Approval.
 */

/**
 * Returns the operations drop down menu.
 *
 * Create the approval dropdown menu items
 * and the callbacks.
 *
 * @return array
 *   An associative array to be used with Drupal Menu API.
 */
function _image_approval_operations($op_type) {
  switch ($op_type) {
    case 'approved':
      $operations = array(
        'disapprove' => array(
          'text' => t('Disapprove the selected images'),
          'callback' => 'image_approval_do_disapprove',
        ),
      );
      break;

    case 'disapproved':
      $operations = array(
        'approve' => array(
          'text' => t('Approve the selected images'),
          'callback' => 'image_approval_do_approval',
        ),
        'delete' => array(
          'text' => t('Delete the selected images'),
          'callback' => 'image_approval_do_delete',
        ),
      );
      break;

    default:
      $operations = array(
        'approve' => array(
          'text' => t('Approve the selected images'),
          'callback' => 'image_approval_do_approval',
        ),
        'disapprove' => array(
          'text' => t('Disapprove the selected images'),
          'callback' => 'image_approval_do_disapprove',
        ),
      );
  }
  return $operations;
}

/**
 * Display administration page.
 *
 * This function displays the image approval administration page.
 * It passes url arg so the form can decide what data it should display.
 *
 * @param string $arg
 *   The section to display. Defaults to "" (empty string), which leads
 *   to awaiting approval section. Other values are "approve" and
 *   "disapprove", respectively.
 */
function image_approval_admin_overview($arg = "") {
  // If user pictures are not enabled.
  if (!variable_get('user_pictures', 0)) {
    if (user_access('access administration pages')) {
      $message = t('User pictures are not !enabled.',
        array('!enabled' => l(t('enabled'), 'admin/user/settings'))
      );
    }
    else {
      $message = t('User pictures are not enabled.');
    }
    drupal_set_message(filter_xss($message));
  }

  return drupal_get_form('image_approval_admin_overview_form', $arg);
}

/**
 * Generate the image approval moderation form.
 *
 * This will display the operations to moderate the images.
 *
 * @param array $form
 *   Will be initialized with the form fields.
 * @param string $type
 *   The currently selected section, used to filter the available
 *   operations.
 *
 * @return array
 *   A system settings form that allows to modify all the
 *   module settings.
 */
function image_approval_admin_overview_form(array $form, $form_state, $type) {

  // The beginings of the Update Options form.
  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
  );
  // Get the operations defined earlier in,
  // the _image_approval_operations function.
  $operations = _image_approval_operations($type);
  $options = array();
  foreach ($operations as $key => $value) {
    // Set each option.
    $options[$key] = $value['text'];
  }

  // Create the options form.
  $form['options']['operation'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => 'approve',
  );
  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  // Load the form's header.
  $header = array(
    'picture' => array(
      'data' => t('Image'),
    ),
    'users' => array(
      'data' => t('User'),
    ),
    'timestamp' => array(
      'data' => t('Time'),
      'field' => 'timestamp',
      'sort' => 'asc',
    ),
    'moderator' => array(
      'data' => t('Moderated by'),
      'field' => 'moderator',
      'sort' => 'desc',
    ),
  );

  $fetch_fids = db_select('file_managed', 'fm')
    ->fields('fm', array('fid'))
    ->execute()
    ->fetchAll();
  $current_fids = array();
  foreach ($fetch_fids as $label => $pair) {
    $current_fids[$label] = $pair->fid;
  }

  // Set the basic SQL Statement.
  $sql = db_select('image_approval', 'im')
    ->extend('TableSort')
    ->extend('PagerDefault')
    ->fields('im', array('timestamp', 'moderator', 'fid'));

  // If $type is NULL, the page is in awaiting approval section.
  // The default menu setting shows all the images that needs to be moderated.
  if ($type == "") {
    $sql = $sql->condition('status', IMAGE_APPROVAL_PENDING, '=');
  }
  elseif ($type == 'disapproved') {
    $sql = $sql->condition('status', IMAGE_APPROVAL_DISAPPROVED, '=');
  }
  elseif ($type == 'approved') {
    $sql = $sql->condition('status', IMAGE_APPROVAL_APPROVED, '=');
  }
  // Order by table header.
  $sql = $sql->orderByHeader($header)->range(0, 25);

  $result = $sql->execute();

  $images = array();
  foreach ($result as $image) {
    if (in_array($image->fid, $current_fids)) {

      if (!$image->moderator) {
        $image->moderator = t('Pending moderation');
      }

      $file = file_load($image->fid);

      $images[$image->fid] = array(
        'picture' => theme('image_style',
        array(
          'style_name' => variable_get('user_picture_style', ''),
          'path' => $file->uri,
          'title' => t('!uri', array('!uri' => $file->uri)),
        )
        ),
        'users' => _image_approval_get_users($image->fid),
        'timestamp' => format_date($image->timestamp, 'short'),
        'moderator' => $image->moderator,
      );
    }
  }

  $form['image_approval'] = array(
    '#type' => 'tableselect',
    '#options' => $images,
    '#header' => $header,
    '#multiple' => TRUE,
    '#empty' => t('No images found.'),
  );
  $form['pager'] = array('#theme' => 'pager');
  return $form;
}

/**
 * Returns a list of usernames associated to a image.
 *
 * @param int $file_id
 *   The file ID of the image.
 *
 * @return array
 *   output consisting the list of username links
 */
function _image_approval_get_users($file_id) {
  $query = db_select('image_approval', 'im')
    ->fields('im', array('uid', 'name'))
    ->condition('uid', 0, '!=')
    ->condition('fid', $file_id, '=');

  $user_results = $query->execute()->fetchAll();
  $results = array();
  foreach ($user_results as $user) {
    $results = l($user->name, 'user/' . $user->uid) . '<br />';
  }

  return $results;
}

/**
 * Implements hook_form_validate().
 */
function image_approval_admin_overview_form_validate($form, $form_state) {
  $count = 0;
  foreach ($form_state['values']['image_approval'] as $key => $value) {
    if (isset($key) && ($value != 0)) {
      $count++;
    }
  }
  if ($count == 0) {
    form_set_error('', t('Please select one or more images to perform the update on.'));
  }
}

/**
 * Implements hook_form_submit().
 */
function image_approval_admin_overview_form_submit($form, $form_state) {

  $edit = $form_state['values'];
  // Get a list of operations.
  $arg = $form_state['build_info']['args'][0];
  $operations = _image_approval_operations($arg);
  // Select the proper callback from the operation.
  // ie: $operation['disapprove']['callback'].
  if ($operations[$edit['operation']]['callback']) {
    $callback = $operations[$edit['operation']]['callback'];
    if (!is_null($callback)) {
      // Then run through all the images that were checked.
      foreach ($edit['image_approval'] as $key => $value) {
        if (!empty($value)) {
          $callback($key);
        }
      }
    }
  }
  cache_clear_all();
  $message = t('The update (%operation) has been performed.', array("%operation" => $edit['operation']));
  drupal_set_message(filter_xss($message));
}
